# Usage

##config
Edit the config file under /etc/nlsf/config.yml
#### FTP Config section
* hostname: (FQDN or IP)
* port: (port FTP is running on)
* username: (the username for the FTP Service)
* password: (your FTP password)
#### Paths config section
* serverlog: path to your latest.log
* restart.log: path to your restart.log
##examples
* display cmd help <br />
nlsf -h --help 
