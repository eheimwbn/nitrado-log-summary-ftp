package main

import (
	"fmt"
	"gitlab.com/eheimwbn/nitrado-log-summary-ftp/internal/ftpwrapper"
	"gitlab.com/eheimwbn/nitrado-log-summary-ftp/internal/readconfig"
	"gitlab.com/eheimwbn/nitrado-log-summary-ftp/internal/searchsubstring"
)

type result struct {
	loggedInUser   []string
	issuedCommands []string
	occurredErrors []string
	deniedActions  []string
}

func main() {

	var result result

	config := readconfig.Readconfig()
	text := ftpwrapper.GetLogContent(config.Ftp.Hostname, config.Ftp.Port, config.Ftp.User, config.Ftp.Password, config.Paths.Serverlog)

	result.loggedInUser = searchsubstring.Searchsubstring(text, "logged in") //
	searchsubstring.Searchsubstring(text, "(D|d)enied")                      // denied
	result.issuedCommands = searchsubstring.Searchsubstring(text, "command:")
	result.occurredErrors = searchsubstring.Searchsubstring(text, "(E|e)rror|Error")

	fmt.Println(result.loggedInUser)
}
