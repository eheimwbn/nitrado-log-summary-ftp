//Package readconfig reads the config.yaml
package readconfig

import (
	"gitlab.com/eheimwbn/nitrado-log-summary-ftp/internal/cmdhelp"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
)

// Setting provides config settings as pseudo object
type Setting struct {
	Ftp struct {
		Hostname string `yaml:"hostname"`
		Port     string `yaml:"port"`
		User     string `yaml:"user"`
		Password string `yaml:"password"`
	}
	Paths struct {
		Serverlog string `yaml:"serverlog"`
		Restart   string `yaml:"restart"`
	}
}

// Readconfig reads the config.yaml file and @return a struct with config settings
func Readconfig() Setting {

	configfile := "/etc/nlsf/config.yml"

	dev := cmdhelp.Cmdhelp()

	if dev {
		configfile = "configs/config.dev.yml"
	}

	yamlconf, err := ioutil.ReadFile(configfile)

	if err != nil {
		log.Printf("yamlFile.Get err   #%v ", err)
	}

	config := Setting{}
	err = yaml.Unmarshal([]byte(yamlconf), &config)
	if err != nil {
		log.Fatalf("error: %v", err)
	}

	return config
}
