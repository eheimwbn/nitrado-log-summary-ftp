// Package ftpwrapper provides the means to fetch the logfiles
package ftpwrapper

import (
	"github.com/jlaffaye/ftp"
	"io/ioutil"
	"log"
)

//GetLogContent connects to the given server and returns a string with the log content
func GetLogContent(host string, port string, user string, password string, logfile string) string {

	url := host + ":" + port
	client, err := ftp.Dial(url)

	if err != nil {
		log.Fatal(err)
	}

	if err = client.Login(user, password); err != nil {
		log.Fatal(err)
	}

	reader, err := client.Retr(logfile)

	if err != nil {
		log.Fatal(err)
	}

	result, err := ioutil.ReadAll(reader)

	logcontent := string(result[:])

	if err != nil {
		log.Fatal(err)
	}

	return logcontent
}
