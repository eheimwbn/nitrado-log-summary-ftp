// Package searchsubsting provides text searches
package searchsubstring

import (
	"regexp"
	"strings"
)

// Searchsubstring takes a regex and a sting, splits the sting into an array and returns a array
// with matching lines
func Searchsubstring(text string, regex string) []string {

	textArray := strings.Split(text, "\n")

	var result []string
	for _, line := range textArray {

		hit, _ := regexp.MatchString(regex, line)
		if hit {
			// debug print
			// fmt.Println(line)
			result = append(result, line)
		}
	}
	return result
}
