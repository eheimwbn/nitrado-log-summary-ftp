// Package cmdhelp package provides a basic CLI cmd help
package cmdhelp

import (
	"fmt"
	"os"
	"strings"
)

// Cmdhelp read cmd ARGs and handel's it flags,
// @return bool if dev als environment is set
func Cmdhelp() bool {
	help := "nlsf\n-h or --help displays this text \n"
	dev := false

	for _, element := range os.Args[1:] {
		if strings.Contains(element, "-h") {
			fmt.Print(help)
		} else if strings.Contains(element, "dev") {
			dev = true
			fmt.Println("running with development settings")
		}

	}
	return dev
}
