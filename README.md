[![License](https://img.shields.io/badge/licens-MIT-green.svg)](https://opensource.org/licenses/MIT)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/eheimwbn/nitrado-log-summary-ftp)](https://goreportcard.com/report/gitlab.com/eheimwbn/nitrado-log-summary-ftp)

# nitrado-log-summary-ftp

fetch and summarise nitrado minecraft logs via FTP.

it is still early WIP

please see also [the Documentation](docs/README.md)